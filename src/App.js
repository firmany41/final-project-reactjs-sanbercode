import HomePage from "./pages/HomePage";
import CreatePage from "./pages/CreatePage";
import UpdatePage from "./pages/UpdatePage";
import TablePage from "./pages/TablePage";
import DetailPage from "./pages/DetailPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ProductProvider } from "./context/ProductContext";
import "./App.css";
import RegisterPage from "./pages/RegisterPage";
import LoginPage from "./pages/LoginPage";
import ProtectedRoute from "./wrapper/ProtectedRoute";
import GuestRoute from "./wrapper/GuestRoute";
import FullProductsPage from "./pages/FullProductsPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <ProductProvider>
          <Routes>
            <Route path="/" element={<HomePage />}></Route>
            <Route path="/product/:id" element={<DetailPage />}></Route>
            <Route path="/products" element={<FullProductsPage />}></Route>

            <Route element={<ProtectedRoute />}>
              <Route path="/create" element={<CreatePage />}></Route>
              <Route path="/update/:id" element={<UpdatePage />}></Route>
              <Route path="/table" element={<TablePage />}></Route>
            </Route>

            <Route element={<GuestRoute />}>
              <Route path="/register" element={<RegisterPage />}></Route>
              <Route path="/login" element={<LoginPage />}></Route>
            </Route>
          </Routes>
        </ProductProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
