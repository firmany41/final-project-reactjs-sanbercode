import React from "react";
import Product from "../components/Product";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import Carousels from "../components/Carousels";

function HomePage() {
  return (
    <div className="bg-gray-100">
      <Helmet>
        <title>Home Page</title>
      </Helmet>

      <Layout>
        <Carousels />
        <Product />
      </Layout>
    </div>
  );
}

export default HomePage;
