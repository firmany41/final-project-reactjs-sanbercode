import React from 'react'
import { Helmet } from 'react-helmet';
import Layout from '../components/Layout';
import RegisterForm from '../components/RegisterForm';

function RegisterPage() {
  return (
    <div>
      <Helmet>
        <title>Register Page</title>
      </Helmet>
      <Layout>
        <RegisterForm />
      </Layout>
    </div>
  );
}

export default RegisterPage
