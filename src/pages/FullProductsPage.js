import React from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import FullProducts from "../components/FullProducts";

function FullProductsPage() {
  return (
    <div className="bg-gray-100">
      <Helmet>
        <title>Products Page</title>
      </Helmet>

      <Layout>
        <FullProducts/>
      </Layout>
    </div>
  );
}

export default FullProductsPage;
