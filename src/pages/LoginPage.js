import React from "react";
import { Helmet } from "react-helmet";
import Layout from "../components/Layout";
import LoginForm from "../components/LoginForm";

function LoginPage() {
  return (
    <div>
      <Helmet>
        <title>Login Page</title>
      </Helmet>
      <Layout>
        <LoginForm />
      </Layout>
    </div>
  );
}

export default LoginPage;
