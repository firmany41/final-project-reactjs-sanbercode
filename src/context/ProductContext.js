import axios from "axios";
import { createContext, useState } from "react";
import { useNavigate } from "react-router-dom";

export const ProductContext = createContext();
export const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState("success");
  const navigate = useNavigate();

  const fetchProducts = async () => {
    try {
      // fetch data
      setLoading(true);
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/final/products",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setProducts(response.data.data);
      console.log(response.data.data);
      setStatus("success");
    } catch (error) {
      console.log(error);
      setStatus("error");
    } finally {
      setLoading(false);
    }
  };

  const moveToCreate = () => {
    navigate("/create");
  };

  return (
    <ProductContext.Provider
      value={{
        products,
        setProducts,
        loading,
        setLoading,
        status,
        setStatus,
        fetchProducts,
        moveToCreate,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};
