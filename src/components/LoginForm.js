import axios from "axios";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button, Form, Input } from "antd";

const rulesSchema = Yup.object().shape({
  email: Yup.string()
    .required("Email harus diisi")
    .email("Email tidak valid"),
  password: Yup.string().required("Password harus diisi"),
});

const yupSync = {
  async validator({ field }, value) {
    await rulesSchema.validateSyncAt(field, { [field]: value });
  },
};

function LoginForm() {
  const navigate = useNavigate();

  const initialState = {
    email: "",
    password: "",
  };

  const onLogin = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/login",
        {
          email: values.email,
          password: values.password,
        }
      );

      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("name", response.data.data.user.name);

      alert("Berhasil Melakukan Login");
      resetForm();
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onLogin,
    validationSchema: rulesSchema,
  });

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-solid border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">Form Login</h1>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          className="max-w-2xl"
        >
          <Form.Item
            label="Email"
            name="email"
            required
            help={touched.email === true && errors.email}
            hasFeedback={true}
            validateStatus={touched.email === true && errors.email != null ? ( "error" ):("")}
          >
            <Input
              type="text"
              placeholder="Masukkan email pengguna"
              onChange={handleChange}
              onBlur={handleBlur}
              name="email"
              value={values.email}
            />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            required
            help={touched.password === true && errors.password}
            hasFeedback={true}
            validateStatus={touched.password === true && errors.password != null ? ( "error" ):("")}
          >
            <Input.Password
              type="text"
              placeholder="Masukkan password"
              onChange={handleChange}
              onBlur={handleBlur}
              name="password"
              value={values.password}
            />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit" onClick={handleSubmit}>
              Login
            </Button>
          </Form.Item>
        </Form>
      </section>
    </div>
  );
}

export default LoginForm;
