import axios from "axios";
import React, { useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button } from "antd";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama Produk wajib diisi"),
  stock: Yup.number().required("Stock Produk wajib diisi"),
  harga: Yup.number().required("Harga Produk wajib diisi"),
  harga_diskon: Yup.number().required(
    "Harga Diskon Produk wajib diisi jika is_diskon true"
  ),
  image_url: Yup.string()
    .required("Image URL Produk wajib diisi")
    .url("Link Gambar tidak valid"),
  category: Yup.mixed().required("Kategori Produk wajib diisi"),
});

function Form({}) {
  const { fetchProducts } = useContext(ProductContext);
  const initialState = {
    name: "",
    harga: 0,
    stock: 0,
    image_url: "",
    is_diskon: false,
    harga_diskon: 0,
    category: "",
    description: "",
  };
  const navigate = useNavigate();

  const storeProduct = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/final/products",
        {
          name: values.name,
          harga: values.harga,
          stock: values.stock,
          image_url: values.image_url,
          is_diskon: values.is_diskon,
          harga_diskon: values.harga_diskon,
          category: values.category,
          description: values.description,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      alert("Berhasil Mengirim Request");
      fetchProducts();
      resetForm();
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
  } = useFormik({
    initialValues: initialState,
    onSubmit: storeProduct,
    validationSchema: rulesSchema,
  });

  return (
    <div className="mt-24 pb-16 mx-20 bg-white shadow-xl">
      <section>
        <h1 className="text-center text-4xl">Form Product</h1>
        <div className="pb-16 mx-16">
          <form className="w-full">
            <div class="grid grid-cols-2 gap-14 px-3">
              <div className="">
                <label
                  htmlFor=""
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
                >
                  Nama :
                </label>
                <input
                  type="text"
                  placeholder="Masukkan Nama Barang"
                  className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="name"
                  value={values.name}
                />
                <p className="text-red-600">
                  {touched.name === true && errors.name}
                </p>
              </div>
              <div className="">
                <label
                  htmlFor=""
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
                >
                  Stock :
                </label>
                <input
                  type="number"
                  placeholder="Masukkan Stock"
                  className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="stock"
                  value={values.stock}
                />
                <p className="text-red-600">
                  {touched.stock === true && errors.stock}
                </p>
              </div>
            </div>
            <div class="grid grid-cols-3 gap-6 py-3 px-3">
              <div className="">
                <label
                  htmlFor=""
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
                >
                  Harga :
                </label>
                <input
                  type="number"
                  placeholder="Masukkan Harga"
                  className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="harga"
                  value={values.harga}
                />
                <p className="text-red-600">
                  {touched.harga === true && errors.harga}
                </p>
              </div>
              <div className="flex items-center mt-5 justify-center">
                <label
                  htmlFor=""
                  className="mr-2 text-sm font-medium text-black-900 dark:text-black-300"
                >
                  Status Diskon :
                </label>
                <input
                  type="checkbox"
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="is_diskon"
                  checked={values.is_diskon}
                />
              </div>
              {values.is_diskon ? (
                <div className="">
                  <label
                    htmlFor=""
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
                  >
                    Harga Diskon :
                  </label>
                  <input
                    type="number"
                    placeholder="Masukkan Harga"
                    className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    name="harga_diskon"
                    value={values.harga_diskon}
                  />
                  <p className="text-red-600">
                    {touched.harga_diskon === true && errors.harga_diskon}
                  </p>
                </div>
              ) : (
                <div></div>
              )}
            </div>
            <div class="grid grid-cols-2 gap-14 pl-3 pr-0">
              <div className="">
                <label
                  htmlFor=""
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
                >
                  Image URL:
                </label>
                <input
                  type="url"
                  placeholder="Masukkan image URL"
                  className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="image_url"
                  value={values.image_url}
                />
                <p className="text-red-600">
                  {touched.image_url === true && errors.image_url}
                </p>
              </div>
              <div className="">
                <label
                  htmlFor=""
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
                >
                  Category :
                </label>
                <select
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="category"
                  id=""
                  className="bg-white-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  value={values.category}
                >
                  <option value="" selected disabled>
                    Pilih Kategori Barang
                  </option>
                  <option value="teknologi">Teknologi</option>
                  <option value="makanan">Makanan</option>
                  <option value="minuman">Minuman</option>
                  <option value="hiburan">Hiburan</option>
                  <option value="kendaraan">Kendaraan</option>
                </select>
                <p className="text-red-600">
                  {touched.category === true && errors.category}
                </p>
              </div>
            </div>
            <div className="grid px-3 pr-8">
              <label
                htmlFor=""
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-black"
              >
                Description :
              </label>
              <input
                type="text"
                placeholder="Masukkan Deskripsi Barang"
                className="block p-5 w-full text-sm text-gray-900 bg-white-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-white-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-blue-500 dark:focus:border-blue-500"
                onChange={handleChange}
                onBlur={handleBlur}
                name="description"
                value={values.description}
              />
            </div>
            <div class="justify-end flex md:order-2 py-3 gap-2">
              <Link to={`/table`}>
              <button
                type="cancel"
                className="px-6 py-2 mt-8  text-blue-700 rounded-md"
              >
                Cancel
              </button>
              </Link>
              <button
                type="submit"
                onClick={handleSubmit}
                className="px-6 py-2 mt-8 bg-blue-600 text-white rounded-md"
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      </section>
    </div>
  );
}

export default Form;
