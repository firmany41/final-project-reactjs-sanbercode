import axios from "axios";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button, Form, Input } from "antd";

const rulesSchema = Yup.object({
  name: Yup.string().required("Nama harus diisi"),
  username: Yup.string().required("Username harus diisi"),
  email: Yup.string()
    .required("Email harus diisi")
    .email("Email tidak valid"),
  password: Yup.string().required("Password harus diisi"),
  password_confirmation: Yup.string().required(
    "Password konfirmasi harus diisi"
  ).oneOf([Yup.ref('password'), null], 'Passwords Berbeda'),
});

function RegisterForm() {
  const navigate = useNavigate();

  const initialState = {
    name: "",
    username: "",
    email: "",
    password: "",
    password_confirmation: "",
  };

  const onRegister = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/register",
        {
          name: values.name,
          username: values.username,
          email: values.email,
          password: values.password,
          password_confirmation: values.password_confirmation,
        }
      );
      alert("Berhasil Register");
      resetForm();
      navigate("/login");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onRegister,
    validationSchema: rulesSchema,
  });

  return (
    <div>
      <section className="max-w-xl mx-auto border-2 border-solid border-gray-600 p-6 mt-12">
        <h1 className="text-center text-2xl">Form Register</h1>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          className="max-w-2xl"
        >
          <Form.Item
            label="Nama"
            name="name"
            help={touched.name === true && errors.name}
            hasFeedback={true}
            validateStatus={touched.name === true && errors.name != null ? ( "error" ):("")}
          >
            <Input
              type="text"
              placeholder="Masukkan nama pengguna"
              onChange={handleChange}
              onBlur={handleBlur}
              name="name"
              value={values.name}
            />
          </Form.Item>
          <Form.Item
            label="Username"
            name="username"
            help={touched.username === true && errors.username}
            hasFeedback={true}
            validateStatus={touched.username === true && errors.username != null ? ( "error" ):("")}
          >
            <Input
              type="text"
              placeholder="Masukkan username pengguna"
              onChange={handleChange}
              onBlur={handleBlur}
              name="username"
              value={values.username}
            />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            help={touched.email === true && errors.email}
            hasFeedback={true}
            validateStatus={touched.email === true && errors.email != null ? ( "error" ):("")}
          >
            <Input
              type="text"
              placeholder="Masukkan email pengguna"
              onChange={handleChange}
              onBlur={handleBlur}
              name="email"
              value={values.email}
            />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            help={touched.password === true && errors.password}
            hasFeedback={true}
            validateStatus={touched.password === true && errors.password != null ? ( "error" ):("")}
          >
            <Input.Password
              type="text"
              placeholder="Masukkan password"
              onChange={handleChange}
              onBlur={handleBlur}
              name="password"
              value={values.password}
            />
          </Form.Item>

          <Form.Item
            label="Konfirmasi Password"
            name="password_confirmation"
            help={
              touched.password_confirmation === true &&
              errors.password_confirmation
            }
            hasFeedback={true}
            validateStatus={touched.password_confirmation === true && errors.password_confirmation != null ? ( "error" ):(touched.password_confirmation === true && errors.password_confirmation == null ? ( "success" ):(""))}
          >
            <Input.Password
              type="text"
              placeholder="Masukkan password"
              onChange={handleChange}
              onBlur={handleBlur}
              name="password_confirmation"
              value={values.password_confirmation}
            />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" onClick={handleSubmit}>
              Register
            </Button>
          </Form.Item>
        </Form>
      </section>
    </div>
  );
}

export default RegisterForm;
