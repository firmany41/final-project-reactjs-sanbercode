import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

function DetailProduct() {
  const { id } = useParams();
  const navigate = useNavigate();

  const [product, setProduct] = useState({
    name: "",
    harga_display: "",
    stock: 0,
    image_url: "",
    harga_diskon_display: "",
    category: "",
    description: "",
    is_diskon: false,
  });
  const fetchProductById = async () => {
    try {
      // fetch data
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      console.log(response.data.data);
      const product = response.data.data;
      // melakukan binding data dari server
      setProduct({
        name: product.name,
        harga_display: product.harga_display,
        stock: product.stock,
        image_url: product.image_url,
        harga_diskon_display: product.harga_diskon_display,
        category: product.category,
        description: product.description,
        is_diskon: product.is_diskon,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    console.log(`Fetch Product id ${id}`);
    fetchProductById();
  }, []);
  return (
    <section className="mt-24 container pb-16 mx-auto bg-white">
      <div className="grid grid-cols-2 gap-6 py-3 px-3">
        <div>
          <img
            src={product.image_url}
            alt=""
            className="w-full h-96 rounded-xl"
          />
        </div>
        <div>
          <h2 className="text-3xl font-medium uppercase mb-2">
            {product.name}
          </h2>
          <div className="flex products-center mb-4">
            <div className="text-xl text-gray-500">{product.category}</div>
          </div>
          {product.is_diskon === true ? (
            <div className="flex flex-col">
              <div>
                <span className="text-2xl text-red-400 line-through">
                  {product.harga_display}
                </span>
              </div>
              <div>
                <span className="text-3xl font-bold text-gray-900 dark:text-black">
                  {product.harga_diskon_display}
                </span>
              </div>
              
            </div>
          ) : (
            <div className="flex flex-col">
              <div>
                <span className="text-3xl font-bold text-gray-900 dark:text-black">
                {product.harga_display}
                </span>
              </div>
            </div>
          )}
          <p className="mt-4 text-gray-600 pb-4 text-xl">
            Deskripsi : {product.description}
          </p>
          <div>
            <span className="text-xl font-medium text-blue-400">
              Stock : {product.stock}
            </span>
          </div>
        </div>
      </div>
    </section>
  );
}

export default DetailProduct;
