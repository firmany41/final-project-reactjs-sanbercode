import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";
import { Button, ConfigProvider, Table } from "antd";

function TableProduct() {
  const { products, fetchProducts, moveToCreate } = useContext(ProductContext);

  const [filter, setFilter] = useState({
    search: "",
    kategori: "",
  });
  const [filterProducts, setFilterProducts] = useState([]);

  const handleChangeInput = (event) => {
    if (event.target.name === "kategori") {
      setFilter({
        ...filter,
        kategori: event.target.value,
      });
    } else if (event.target.name === "search") {
      setFilter({
        ...filter,
        search: event.target.value,
      });
    }
  };

  const handleSearch = () => {
    console.log(filter);
    console.log(products);
    let tempProducts = structuredClone(products);
    if (filter.search !== "") {
      tempProducts = tempProducts.filter((product) => {
        return product.name.toLowerCase().includes(filter.search.toLowerCase());
      });
    }
    if (filter.kategori !== "") {
      tempProducts = tempProducts.filter((product) => {
        return product.category === filter.kategori;
      });
    }
    setFilterProducts(tempProducts);
  };

  const handleReset = () => {
    setFilter({
      search: "",
      kategori: "",
    });
    setFilterProducts(products);
  };

  useEffect(() => {
    setFilterProducts(products);
  }, [products]);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Nama Barang",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Harga",
      dataIndex: "harga_display",
      key: "harga_display",
    },
    {
      title: "Harga Diskon",
      dataIndex: "harga_diskon_display",
      key: "harga_diskon_display",
    },
    {
      title: "Status Diskon",
      dataIndex: "status_diskon",
      key: "status_diskon",
      render: (_, record) => {
        if (record.is_diskon === true) {
          return <>Aktif</>;
        } else {
          return <>Mati</>;
        }
      },
    },
    {
      title: "Image",
      dataIndex: "image_url",
      key: "image_url",
      render: (_, record) => {
        return (
          <img
            className="w-32 h-32 object-cover rounded-xl "
            src={record.image_url}
            alt={record.name + " Image"}
          />
        );
      },
    },
    {
      title: "Kategori",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Dibuat Oleh",
      dataIndex: "author",
      key: "author",
      render: (_, record) => {
        if (record.user === null) {
          return <>-</>;
        }
        return <>{record.user.name}</>;
      },
    },

    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (_, record) => {
        return (
          <div className="flex gap-2">
            <Link to={`/update/${record.id}`}>
              <ConfigProvider
                theme={{
                  token: {
                    colorPrimary: "#da8e01",
                  },
                }}
              >
                <Button type="primary">Update</Button>
              </ConfigProvider>
            </Link>
            <ConfigProvider
              theme={{
                token: {
                  colorPrimary: "#b90000",
                },
              }}
            >
              <Button type="primary" onClick={() => onDelete(record.id)}>
                Delete
              </Button>
            </ConfigProvider>
          </div>
        );
      },
    },
  ];

  const onUpdate = (product) => {};
  const onDelete = async (id) => {
    try {
      let deleteCon = window.confirm("Apakah Anda Yakin Menghapus Produk Ini?");
      if (deleteCon == true) {
        // fetch data
        const response = await axios.delete(
          `https://api-project.amandemy.co.id/api/final/products/${id}`,
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );
        alert("Berhasil Menghapus Product");
        fetchProducts();
      } else {
        alert("Batal Menghapus Product");
      }
    } catch (error) {
      console.log(error);
      alert("Gagal Menghapus Product");
    }
  };
  return (
    <section>
      <h1 className="my-8 text-3xl font-bold text-center ">Table Product</h1>
      <div className="max-w-7xl mx-auto w-full my-4 ">
        <div class="justify-end flex">
          <Button
            type="dashed"
            onClick={moveToCreate}
            className="w-36 font-semibold bg-gray-200"
          >
            Create Product +
          </Button>
        </div>
        <div class="justify-end flex gap-1 py-2">
          <select
            value={filter.kategori}
            onChange={handleChangeInput}
            name="kategori"
            id=""
            className="rounded-lg"
          >
            <option value="">Kategori Barang</option>
            <option value="teknologi">Teknologi</option>
            <option value="makanan">Makanan</option>
            <option value="minuman">Minuman</option>
            <option value="hiburan">Hiburan</option>
            <option value="kendaraan">Kendaraan</option>
          </select>

          <input
            value={filter.search}
            onChange={handleChangeInput}
            name="search"
            placeholder="Masukkan Nama Barang"
            type="text"
            className="rounded-lg"
          />

          <Button onClick={handleSearch} type="primary" htmlType="submit">
            Search
          </Button>
          <Button onClick={handleReset} type="primary" danger>
            Reset
          </Button>
        </div>
        <Table columns={columns} dataSource={filterProducts} bordered />
      </div>
    </section>
  );
}

export default TableProduct;
