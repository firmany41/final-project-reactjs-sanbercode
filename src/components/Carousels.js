import { Carousel} from "antd";
import React from "react";

const contentStyle = {
    margin: 0,
    height: '500px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    backgroundImage: 'url("https://api-project.amandemy.co.id/images/sepeda.jpg")',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
   backgroundAttachment: 'fixed',
  };
  const contentStyle2 = {
    margin: 0,
    height: '500px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    backgroundImage: 'url("https://api-project.amandemy.co.id/images/headphone.jpg")',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
   backgroundAttachment: 'fixed',
  };
function Carousels() {
  return (
    <Carousel  autoplay>
      <div>
        <h3 style={contentStyle}></h3>
      </div>
      <div>
        <h3 style={contentStyle2}></h3>
      </div>
    </Carousel>
  );
}
export default Carousels;
