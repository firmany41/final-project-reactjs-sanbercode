import React, { Fragment } from "react";
import Navbar from "./Navbar";

function Layout({ children }) {
  return (
    <div>
      <Navbar />
      <Fragment>{children}</Fragment>
    </div>
  );
}

export default Layout;
