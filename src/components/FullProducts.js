import React, { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";
import { Button, Card } from "antd";

function FullProducts() {
  const { products, fetchProducts, loading, status } =
    useContext(ProductContext);

  useEffect(() => {
    fetchProducts();
  }, []);
  return (
    <div>
      <section className="mx-20">
        <h1 className="text-3xl font-bold">Catalog Products</h1>
        {loading === false ? (
          <div className="grid grid-cols-4 pb-4 gap-8">
            {products.map((product, index) => (
              <Link to={`/product/${product.id}`} className="no-underline">
                <Card
                  hoverable
                  className="rounded-xl"
                  style={{
                    width: 280,
                    height: 430,
                  }}
                  cover={
                    <img
                      className="w-full h-64 object-cover rounded-xl"
                      alt="Product Image"
                      src={product.image_url}
                    />
                  }
                >
                  <div>
                    <h1
                      className="my-1 title-h1"
                    >
                      {product.name}
                    </h1>

                    {product.is_diskon === true ? (
                      <div>
                        <p className="line-through text-red-500  my-1">
                          {product.harga_display}
                        </p>
                        <p className="text-2xl my-1">
                          {product.harga_diskon_display}
                        </p>
                      </div>
                    ) : (
                      <div>
                        <p className="text-2xl my-1">{product.harga_display}</p>
                      </div>
                    )}

                    <p className="text-blue-400 my-2">
                      Stock : {product.stock}
                    </p>
                  </div>
                </Card>
              </Link>
            ))}
          </div>
        ) : (
          <h1 className="text-center my-6 text-3xl font-bold">Loading ....</h1>
        )}
      </section>
    </div>
  );
}

export default FullProducts;
