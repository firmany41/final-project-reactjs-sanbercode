import React from "react";
import logo from "../img/logo.svg";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import { Button, Dropdown, Space } from "antd";
import { DownOutlined } from "@ant-design/icons";

function Navbar() {
  const navigate = useNavigate();
  const onLogout = async () => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/logout",
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    } finally {
      localStorage.removeItem("token");
      localStorage.removeItem("name");
      navigate("/");
    }
  };
  return (
    <header className="shadow-lg py-4 bg-white px-12 sticky top-0 z-20 h-8 items-center">
      <nav className="mx-auto max-w-7xl flex justify-between h-5">
        <div className="flex items-center">
          <a href="/">
            <img src={logo} className="w-32" alt="Logo Trialshop" />
          </a>
        </div>
        {localStorage.getItem("token") != null ? (
          <>
            <ul className="flex items-end gap-4 text-blue-500 text-2xl list-none justify-center">
              <Link
                className="no-underline text-blue-500 hover:text-violet-900"
                to="/"
              >
                <li>Home</li>
              </Link>
              <Link
                className="no-underline text-blue-500 hover:text-violet-900"
                to="/products"
              >
                <li>Products</li>
              </Link>
              <Link
                className="no-underline text-blue-500 hover:text-violet-900"
                to="/table"
              >
                <li>Table</li>
              </Link>
            </ul>
          </>
        ) : (
          <>
            <ul className="flex items-end gap-4 text-2xl list-none justify-center">
              <Link
                className="no-underline text-blue-500 hover:text-violet-900"
                to="/"
              >
                <li>Home</li>
              </Link>
              <Link
                className="no-underline text-blue-500 hover:text-violet-900"
                to="/products"
              >
                <li>Products</li>
              </Link>
              <Link
                className="no-underline text-blue-500 hover:text-violet-900"
                to="/table"
              >
                <li>Table</li>
              </Link>
            </ul>
          </>
        )}

        <div className="flex items-center gap-4 text-cyan-500 text-xl">
          {localStorage.getItem("token") != null ? (
            <>
              <div className="flex items-center gap-4 w-40">
                <Dropdown
                  dropdownRender={() => (
                    <Space
                      style={{
                        padding: 8,
                        backgroundColor: "white",
                      }}
                    >
                      <Button onClick={onLogout} type="primary" danger>
                        Logout
                      </Button>
                    </Space>
                  )}
                >
                  <a onClick={(e) => e.preventDefault()}>
                    <Space>
                      <p>Hai, {localStorage.getItem("name")}</p>
                      <DownOutlined />
                    </Space>
                  </a>
                </Dropdown>
              </div>
            </>
          ) : (
            <>
              <Link className="no-underline" to="/login">
                <Button type="primary">Login</Button>
              </Link>
              <Link className="no-underline" to="/register">
                <Button>Register</Button>
              </Link>
            </>
          )}
        </div>
      </nav>
    </header>
  );
}

export default Navbar;
